package com.netcracker.barkun.service;

import com.netcracker.barkun.entity.Book;

public interface BookService {

    Book create(Book book);

    Book find(Long id);

    Book update(Book book);

    void remove(Long id);
}

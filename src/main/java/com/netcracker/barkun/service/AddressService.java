package com.netcracker.barkun.service;

import com.netcracker.barkun.entity.Address;

public interface AddressService {

    Address create(Address address);

    Address find(Long id);

    Address update(Address address);

    void remove(Long id);
}

package com.netcracker.barkun.service;

import com.netcracker.barkun.entity.Role;

public interface RoleService {

    Role create(Role role);

    Role find(Long id);

    Role update(Role role);

    void remove(Long id);
}

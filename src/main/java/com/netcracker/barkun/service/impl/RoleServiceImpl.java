package com.netcracker.barkun.service.impl;

import com.netcracker.barkun.dao.RoleDao;
import com.netcracker.barkun.entity.Role;
import com.netcracker.barkun.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleDao roleDao;

    @Autowired
    private RoleServiceImpl(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public Role create(Role role) {
        return roleDao.create(role);
    }

    @Override
    public Role find(Long id) {
        return roleDao.find(id);
    }

    @Override
    public Role update(Role role) {
        return roleDao.update(role);
    }

    @Override
    public void remove(Long id) {
        roleDao.remove(id);
    }
}

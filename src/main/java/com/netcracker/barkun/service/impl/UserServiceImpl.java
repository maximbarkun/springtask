package com.netcracker.barkun.service.impl;

import com.netcracker.barkun.dao.UserDao;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.User;
import com.netcracker.barkun.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;

    @Autowired
    private UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public User create(User user) {
        return userDao.create(user);
    }

    @Override
    public User find(Long id) {
        return userDao.find(id);
    }

    @Override
    public User update(User user) {
        return userDao.update(user);
    }

    @Override
    public User addBook(Book book, User user) {
        return userDao.addBook(book, user);
    }

    @Override
    public User addBookList(List<Book> books, User user) {
        return userDao.addBookList(books, user);
    }

    @Override
    public void remove(Long id) {
        userDao.remove(id);
    }
}

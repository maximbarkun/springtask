package com.netcracker.barkun.service.impl;

import com.netcracker.barkun.dao.AddressDao;
import com.netcracker.barkun.entity.Address;
import com.netcracker.barkun.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    private final AddressDao addressDao;

    @Autowired
    public AddressServiceImpl(AddressDao addressDao) {
        this.addressDao = addressDao;
    }

    @Override
    public Address create(Address address) {
        return addressDao.create(address);
    }

    @Override
    public Address find(Long id) {
        return addressDao.find(id);
    }

    @Override
    public Address update(Address address) {
        return addressDao.update(address);
    }

    @Override
    public void remove(Long id) {
        addressDao.remove(id);
    }
}

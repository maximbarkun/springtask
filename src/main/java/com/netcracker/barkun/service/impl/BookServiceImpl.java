package com.netcracker.barkun.service.impl;

import com.netcracker.barkun.dao.BookDao;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService {

    private final BookDao bookDao;

    @Autowired
    private BookServiceImpl(BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @Override
    public Book create(Book book) {
        return bookDao.create(book);
    }

    @Override
    public Book find(Long id) {
        return bookDao.find(id);
    }

    @Override
    public Book update(Book book) {
        return bookDao.update(book);
    }

    @Override
    public void remove(Long id) {
        bookDao.remove(id);
    }
}

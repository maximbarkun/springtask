package com.netcracker.barkun.service;

import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.User;

import java.util.List;

public interface UserService {

    User create(User user);

    User find(Long id);

    User update(User user);

    User addBook(Book book, User user);

    User addBookList(List<Book> books, User user);

    void remove(Long id);
}

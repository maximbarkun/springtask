package com.netcracker.barkun.dao;

import com.netcracker.barkun.entity.Role;

public interface RoleDao {

    Role create(Role role);

    Role find(Long id);

    Role update(Role role);

    void remove(Long id);
}

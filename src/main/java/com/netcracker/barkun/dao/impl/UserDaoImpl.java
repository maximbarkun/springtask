package com.netcracker.barkun.dao.impl;

import com.netcracker.barkun.dao.UserDao;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.User;
import com.netcracker.barkun.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    @Override
    public User create(User user) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(user);
        tx.commit();
        return user;
    }

    @Override
    public User find(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        User foundUser;
        tx.begin();
        foundUser = entityManager.find(User.class, id);
        tx.commit();
        return foundUser;
    }

    @Override
    public User update(User user) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(user);
        tx.commit();
        return user;
    }

    @Override
    public User addBook(Book book, User user) {
        EntityTransaction tx = entityManager.getTransaction();
        user.getBooks().add(book);
        tx.begin();
        entityManager.merge(user);
        tx.commit();
        return user;
    }

    @Override
    public User addBookList(List<Book> books, User user) {
        EntityTransaction tx = entityManager.getTransaction();
        for (Book iterator: books){
            user.getBooks().add(iterator);
        }
        tx.begin();
        entityManager.merge(user);
        tx.commit();
        return user;
    }

    @Override
    public void remove(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        User removeUser;
        tx.begin();
        removeUser = entityManager.getReference(User.class, id);
        entityManager.remove(removeUser);
        tx.commit();
    }
}

package com.netcracker.barkun.dao.impl;

import com.netcracker.barkun.dao.RoleDao;
import com.netcracker.barkun.entity.Role;
import com.netcracker.barkun.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class RoleDaoImpl implements RoleDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    @Override
    public Role create(Role role) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(role);
        tx.commit();
        return role;
    }

    @Override
    public Role find(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Role foundRole;
        tx.begin();
        foundRole = entityManager.find(Role.class, id);
        tx.commit();
        return foundRole;
    }

    @Override
    public Role update(Role role) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(role);
        tx.commit();
        return role;
    }

    @Override
    public void remove(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Role removeRole;
        tx.begin();
        removeRole = entityManager.getReference(Role.class, id);
        entityManager.remove(removeRole);
        tx.commit();
    }
}

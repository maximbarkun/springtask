package com.netcracker.barkun.dao.impl;

import com.netcracker.barkun.dao.BookDao;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class BookDaoImpl implements BookDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    @Override
    public Book create(Book book) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(book);
        tx.commit();
        return book;
    }

    @Override
    public Book find(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Book foundBook;
        tx.begin();
        foundBook = entityManager.find(Book.class, id);
        tx.commit();
        return foundBook;
    }

    @Override
    public Book update(Book book) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(book);
        tx.commit();
        return book;
    }

    @Override
    public void remove(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Book removeBook;
        tx.begin();
        removeBook = entityManager.getReference(Book.class, id);
        entityManager.remove(removeBook);
        tx.commit();
    }
}


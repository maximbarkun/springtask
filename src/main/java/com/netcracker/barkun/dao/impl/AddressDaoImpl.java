package com.netcracker.barkun.dao.impl;

import com.netcracker.barkun.dao.AddressDao;
import com.netcracker.barkun.entity.Address;
import com.netcracker.barkun.utils.PostgreSQLDatabaseManager;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class AddressDaoImpl implements AddressDao {

    private EntityManager entityManager = PostgreSQLDatabaseManager.getInstance().getEntityManager();

    @Override
    public Address create(Address address) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.persist(address);
        tx.commit();
        return address;
    }

    @Override
    public Address find(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Address foundAddress;
        tx.begin();
        foundAddress = entityManager.find(Address.class, id);
        tx.commit();
        return foundAddress;
    }

    @Override
    public Address update(Address address) {
        EntityTransaction tx = entityManager.getTransaction();
        tx.begin();
        entityManager.merge(address);
        tx.commit();
        return address;
    }

    @Override
    public void remove(Long id) {
        EntityTransaction tx = entityManager.getTransaction();
        Address removeAddress;
        tx.begin();
        removeAddress = entityManager.getReference(Address.class, id);
        entityManager.remove(removeAddress);
        tx.commit();
    }
}

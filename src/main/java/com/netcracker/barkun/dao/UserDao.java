package com.netcracker.barkun.dao;

import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.User;

import java.util.List;

public interface UserDao {

    User create(User user);

    User find(Long id);

    User update(User user);

    User addBook(Book book, User user);

    User addBookList(List<Book> books, User user);

    void remove(Long id);
}

package com.netcracker.barkun.dao;


import com.netcracker.barkun.entity.Address;

public interface AddressDao {

    Address create(Address address);

    Address find(Long id);

    Address update(Address address);

    void remove(Long id);
}

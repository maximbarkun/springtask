package com.netcracker.barkun.dao;

import com.netcracker.barkun.entity.Book;

public interface BookDao {

    Book create(Book book);

    Book find(Long id);

    Book update(Book book);

    void remove(Long id);
}

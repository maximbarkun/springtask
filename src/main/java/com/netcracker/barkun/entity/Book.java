package com.netcracker.barkun.entity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.Objects;
import java.util.StringJoiner;

@Entity
public class Book extends BaseEntity {

    private String tittle;

    private String author;

    private Date dateOfPublication;

    @ManyToOne
    private User user;

    public Book() {
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getDateOfPublication() {
        return dateOfPublication;
    }

    public void setDateOfPublication(Date dateOfPublication) {
        this.dateOfPublication = dateOfPublication;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(tittle, book.tittle) &&
                Objects.equals(author, book.author) &&
                Objects.equals(dateOfPublication, book.dateOfPublication) &&
                Objects.equals(user, book.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tittle, author, dateOfPublication, user);
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Book.class.getSimpleName() + "[", "]")
                .add("tittle='" + tittle + "'")
                .add("author='" + author + "'")
                .add("dateOfPublication=" + dateOfPublication)
                .add("user=" + user)
                .toString();
    }
}

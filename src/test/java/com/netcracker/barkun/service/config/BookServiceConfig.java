package com.netcracker.barkun.service.config;

import com.netcracker.barkun.dao.BookDao;
import com.netcracker.barkun.dao.impl.BookDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.barkun.service.impl")
public class BookServiceConfig {

    @Bean
    public BookDao bookService() {
        return new BookDaoImpl();
    }
}

package com.netcracker.barkun.service.config;

import com.netcracker.barkun.dao.AddressDao;
import com.netcracker.barkun.dao.impl.AddressDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.barkun.service.impl")
public class AddressServiceConfig {

    @Bean
    public AddressDao addressService(){
        return new AddressDaoImpl();
    }
}

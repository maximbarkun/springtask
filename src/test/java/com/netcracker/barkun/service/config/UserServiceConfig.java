package com.netcracker.barkun.service.config;

import com.netcracker.barkun.dao.UserDao;
import com.netcracker.barkun.dao.impl.UserDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.barkun.service.impl")
public class UserServiceConfig {

    @Bean
    public UserDao userService(){ return new UserDaoImpl(); }
}

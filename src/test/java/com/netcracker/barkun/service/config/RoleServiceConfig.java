package com.netcracker.barkun.service.config;

import com.netcracker.barkun.dao.RoleDao;
import com.netcracker.barkun.dao.impl.RoleDaoImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.netcracker.barkun.service.impl")
public class RoleServiceConfig {

    @Bean
    public RoleDao roleService() {
        return new RoleDaoImpl();
    }
}

package com.netcracker.barkun.service;

import com.netcracker.barkun.dao.impl.RoleDaoImpl;
import com.netcracker.barkun.entity.Role;
import com.netcracker.barkun.service.config.AddressServiceConfig;
import com.netcracker.barkun.service.config.BookServiceConfig;
import com.netcracker.barkun.service.config.RoleServiceConfig;
import com.netcracker.barkun.service.config.UserServiceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AddressServiceConfig.class, UserServiceConfig.class, BookServiceConfig.class, RoleServiceConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class TestRoleServiceImpl {

    @Autowired
    private RoleService roleService;

    private Role role;

    private Role createdRole;
    private Role removeRole;
    private Role foundRole;
    private Role updateRole;

    @BeforeEach
    public void setUp() {
        role = new Role();
        role.setName("User");
    }

    @Test
    public void testCreateRole() {
        createdRole = roleService.create(role);

        assertNotNull(createdRole);
        assertEquals(createdRole.getName(), role.getName());
    }

    @Test
    public void testRemoveRole() {
        createdRole = roleService.create(role);
        roleService.remove(role.getId());

        removeRole = roleService.find(role.getId());

        assertNull(removeRole);
    }

    @Test
    public void testFindRoleById() {
        createdRole = roleService.create(role);

        foundRole = roleService.find(role.getId());

        assertEquals(createdRole, foundRole);
    }

    @Test
    public void testUpdateRole() {

        createdRole = roleService.create(role);
        role.setName("Admin");
        updateRole = roleService.update(role);

        assertEquals(role.getName(), updateRole.getName());
    }
}

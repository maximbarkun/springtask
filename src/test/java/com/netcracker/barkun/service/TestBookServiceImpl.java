package com.netcracker.barkun.service;

import com.netcracker.barkun.dao.impl.BookDaoImpl;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.service.config.AddressServiceConfig;
import com.netcracker.barkun.service.config.BookServiceConfig;
import com.netcracker.barkun.service.config.RoleServiceConfig;
import com.netcracker.barkun.service.config.UserServiceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AddressServiceConfig.class, UserServiceConfig.class, BookServiceConfig.class, RoleServiceConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class TestBookServiceImpl {

    @Autowired
    private BookService bookService;

    private Book book;

    private Book createdBook;
    private Book removeBook;
    private Book foundBook;
    private Book updateBook;

    @BeforeEach
    public void setUp() {
        book = new Book();
        book.setAuthor("Pushkin");
        book.setTittle("Dybrovskiu");
        Date date = new Date(1841);
        book.setDateOfPublication(date);
    }

    @Test
    public void testCreateAddress() {

        createdBook = bookService.create(book);

        assertNotNull(createdBook);
        assertEquals(createdBook, book);
    }

    @Test
    public void testRemoveAddress() {

        createdBook = bookService.create(book);
        bookService.remove(book.getId());

        removeBook = bookService.find(book.getId());

        assertNull(removeBook);
    }

    @Test
    public void testFindAddressById() {
        createdBook = bookService.create(book);

        foundBook = bookService.find(book.getId());

        assertEquals(createdBook, foundBook);
    }

    @Test
    public void testUpdateRole() {

        bookService.create(book);
        book.setAuthor("Lermontov");
        updateBook = bookService.update(book);

        assertEquals(book.getAuthor(), updateBook.getAuthor());
    }
}

package com.netcracker.barkun.service;

import com.netcracker.barkun.entity.Address;
import com.netcracker.barkun.service.config.AddressServiceConfig;
import com.netcracker.barkun.service.config.BookServiceConfig;
import com.netcracker.barkun.service.config.RoleServiceConfig;
import com.netcracker.barkun.service.config.UserServiceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AddressServiceConfig.class, UserServiceConfig.class, BookServiceConfig.class, RoleServiceConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class TestAddressServiceImpl {

    @Autowired
    private AddressService addressService;

    private Address address;

    private Address createdAddress;
    private Address removeAddress;
    private Address foundAddress;
    private Address updateRole;

    @BeforeEach
    public void setUp() {
        address = new Address();
        address.setCity("Minsk");
        address.setStreet("Gogalya");
        address.setHouseNumber(55);
    }

    @Test
    public void testCreateAddress() {

        createdAddress = addressService.create(address);

        assertNotNull(createdAddress);
        assertEquals(createdAddress, address);
    }

    @Test
    public void testRemoveAddress() {

        createdAddress = addressService.create(address);
        addressService.remove(address.getId());

        removeAddress = addressService.find(address.getId());

        assertNull(removeAddress);
    }

    @Test
    public void testFindAddressById() {
        createdAddress = addressService.create(address);

        foundAddress = addressService.find(address.getId());

        assertEquals(createdAddress, foundAddress);
    }

    @Test
    public void testUpdateRole() {

        addressService.create(address);
        address.setCity("Riga");
        updateRole = addressService.update(address);

        assertEquals(address.getCity(), updateRole.getCity());
    }
}

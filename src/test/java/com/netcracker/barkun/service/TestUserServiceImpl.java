package com.netcracker.barkun.service;

import com.netcracker.barkun.dao.impl.UserDaoImpl;
import com.netcracker.barkun.entity.Address;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.Role;
import com.netcracker.barkun.entity.User;
import com.netcracker.barkun.service.config.AddressServiceConfig;
import com.netcracker.barkun.service.config.BookServiceConfig;
import com.netcracker.barkun.service.config.RoleServiceConfig;
import com.netcracker.barkun.service.config.UserServiceConfig;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {AddressServiceConfig.class, UserServiceConfig.class, BookServiceConfig.class, RoleServiceConfig.class},
        loader = AnnotationConfigContextLoader.class)
public class TestUserServiceImpl {

    @Autowired
    private UserService userService;

    private User user;

    private User createdUser;
    private User removeUser;
    private User foundUser;
    private User updateUser;

    @BeforeEach
    public void setUp() {

        Address address = new Address();
        address.setCity("Minsk");
        address.setStreet("Gogalya");
        address.setHouseNumber(55);

        Book book = new Book();
        book.setAuthor("Pushkin");
        book.setTittle("Dybrovskiu");
        book.setDateOfPublication(new Date(1841));

        Role role = new Role();
        role.setName("User");

        user = new User();
        user.setName("Maxim");
        user.setAddress(address);
        user.setEmail("barkun@mail.ru");
        user.setSurname("Barkun");
        user.setRole(role);
        user.getBooks().add(book);
    }

    @Test
    public void testCreateUser() {
        createdUser = userService.create(user);

        assertNotNull(createdUser);
        assertEquals(createdUser.getName(), user.getName());
    }

    @Test
    public void testRemoveUser() {
        createdUser = userService.create(user);
        userService.remove(user.getId());

        removeUser = userService.find(user.getId());

        assertNull(removeUser);
    }

    @Test
    public void testFindUserById() {
        createdUser = userService.create(user);

        foundUser = userService.find(user.getId());

        assertEquals(createdUser, foundUser);
    }

    @Test
    public void testUpdateUser() {

        createdUser = userService.create(user);
        user.setName("Admin");
        updateUser = userService.update(user);

        assertEquals(user.getName(), updateUser.getName());
    }

    @Test
    public void testAddBook() {

        userService.create(user);

        Book book2 = new Book();
        book2.setAuthor("Lermontov");
        book2.setTittle("Капитанская дочка");
        book2.setDateOfPublication(new Date(1836));


        updateUser = userService.addBook(book2, user);

        assertEquals(user.getBooks(), updateUser.getBooks());
    }

    @Test
    public void testAddBookList() {

        userService.create(user);

        Book book2 = new Book();
        book2.setAuthor("Tolstoy");
        book2.setTittle("Капитанская дочка");
        book2.setDateOfPublication(new Date(1836));

        List<Book> books = new ArrayList<>();
        books.add(book2);
        book2.setAuthor("Stiven");
        books.add(book2);

        updateUser = userService.addBookList(books, user);

        assertEquals(user.getBooks(), updateUser.getBooks());
    }
}

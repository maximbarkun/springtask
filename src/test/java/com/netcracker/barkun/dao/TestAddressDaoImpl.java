package com.netcracker.barkun.dao;

import com.netcracker.barkun.dao.impl.AddressDaoImpl;
import com.netcracker.barkun.entity.Address;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestAddressDaoImpl {

    private AddressDao addressDao;

    private Address address;

    private Address createdAddress;
    private Address removeAddress;
    private Address foundAddress;
    private Address updateRole;

    @BeforeEach
    public void setUp() {
        addressDao = new AddressDaoImpl();
        address = new Address();
        address.setCity("Minsk");
        address.setStreet("Gogalya");
        address.setHouseNumber(55);
    }

    @Test
    public void testCreateAddress() {

        createdAddress = addressDao.create(address);

        assertNotNull(createdAddress);
        assertEquals(createdAddress, address);
    }

    @Test
    public void testRemoveAddress() {

        createdAddress = addressDao.create(address);
        addressDao.remove(address.getId());

        removeAddress = addressDao.find(address.getId());

        assertNull(removeAddress);
    }

    @Test
    public void testFindAddressById() {
        createdAddress = addressDao.create(address);

        foundAddress = addressDao.find(address.getId());

        assertEquals(createdAddress, foundAddress);
    }

    @Test
    public void testUpdateRole() {

        addressDao.create(address);
        address.setCity("Riga");
        updateRole = addressDao.update(address);

        assertEquals(address.getCity(), updateRole.getCity());
    }
}

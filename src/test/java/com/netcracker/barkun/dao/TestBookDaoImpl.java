package com.netcracker.barkun.dao;

import com.netcracker.barkun.dao.impl.BookDaoImpl;
import com.netcracker.barkun.entity.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestBookDaoImpl {

    private BookDao bookDao;

    private Book book;

    private Book createdBook;
    private Book removeBook;
    private Book foundBook;
    private Book updateBook;

    @BeforeEach
    public void setUp() {
        bookDao = new BookDaoImpl();
        book = new Book();
        book.setAuthor("Pushkin");
        book.setTittle("Dybrovskiu");
        Date date = new Date(1841);
        book.setDateOfPublication(date);
    }

    @Test
    public void testCreateAddress() {

        createdBook = bookDao.create(book);

        assertNotNull(createdBook);
        assertEquals(createdBook, book);
    }

    @Test
    public void testRemoveAddress() {

        createdBook = bookDao.create(book);
        bookDao.remove(book.getId());

        removeBook = bookDao.find(book.getId());

        assertNull(removeBook);
    }

    @Test
    public void testFindAddressById() {
        createdBook = bookDao.create(book);

        foundBook = bookDao.find(book.getId());

        assertEquals(createdBook, foundBook);
    }

    @Test
    public void testUpdateRole() {

        bookDao.create(book);
        book.setAuthor("Lermontov");
        updateBook = bookDao.update(book);

        assertEquals(book.getAuthor(), updateBook.getAuthor());
    }
}

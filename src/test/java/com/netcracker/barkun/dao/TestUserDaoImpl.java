package com.netcracker.barkun.dao;

import com.netcracker.barkun.dao.impl.UserDaoImpl;
import com.netcracker.barkun.entity.Address;
import com.netcracker.barkun.entity.Book;
import com.netcracker.barkun.entity.Role;
import com.netcracker.barkun.entity.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestUserDaoImpl {

    private UserDao userDao;

    private User user;

    private User createdUser;
    private User removeUser;
    private User foundUser;
    private User updateUser;

    @BeforeEach
    public void setUp() {
        userDao = new UserDaoImpl();

        Address address = new Address();
        address.setCity("Minsk");
        address.setStreet("Gogalya");
        address.setHouseNumber(55);

        Book book = new Book();
        book.setAuthor("Pushkin");
        book.setTittle("Dybrovskiu");
        book.setDateOfPublication(new Date(1841));

        Role role = new Role();
        role.setName("User");

        user = new User();
        user.setName("Maxim");
        user.setAddress(address);
        user.setEmail("barkun@mail.ru");
        user.setSurname("Barkun");
        user.setRole(role);
        user.getBooks().add(book);
    }

    @Test
    public void testCreateUser() {
        createdUser = userDao.create(user);

        assertNotNull(createdUser);
        assertEquals(createdUser.getName(), user.getName());
    }

    @Test
    public void testRemoveUser() {
        createdUser = userDao.create(user);
        userDao.remove(user.getId());

        removeUser = userDao.find(user.getId());

        assertNull(removeUser);
    }

    @Test
    public void testFindUserById() {
        createdUser = userDao.create(user);

        foundUser = userDao.find(user.getId());

        assertEquals(createdUser, foundUser);
    }

    @Test
    public void testUpdateUser() {

        createdUser = userDao.create(user);
        user.setName("Admin");
        updateUser = userDao.update(user);

        assertEquals(user.getName(), updateUser.getName());
    }

    @Test
    public void testAddBook() {

        userDao.create(user);

        Book book2 = new Book();
        book2.setAuthor("Pushkin");
        book2.setTittle("Капитанская дочка");
        book2.setDateOfPublication(new Date(1836));


        updateUser = userDao.addBook(book2, user);

        assertEquals(user.getBooks(), updateUser.getBooks());
    }

    @Test
    public void testAddBookList() {

        userDao.create(user);

        Book book2 = new Book();
        book2.setAuthor("Pushkin");
        book2.setTittle("Капитанская дочка");
        book2.setDateOfPublication(new Date(1836));

        List<Book> books = new ArrayList<>();
        books.add(book2);
        book2.setAuthor("Stiven");
        books.add(book2);

        updateUser = userDao.addBookList(books, user);

        assertEquals(user.getBooks(), updateUser.getBooks());
    }
}

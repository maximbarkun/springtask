package com.netcracker.barkun.dao;

import com.netcracker.barkun.dao.impl.RoleDaoImpl;
import com.netcracker.barkun.entity.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class TestRoleDaoImpl {

    private RoleDao roleDao;

    private Role role;

    private Role createdRole;
    private Role removeRole;
    private Role foundRole;
    private Role updateRole;

    @BeforeEach
    public void setUp() {
        roleDao = new RoleDaoImpl();
        role = new Role();
        role.setName("User");
    }

    @Test
    public void testCreateRole() {
        createdRole = roleDao.create(role);

        assertNotNull(createdRole);
        assertEquals(createdRole.getName(), role.getName());
    }

    @Test
    public void testRemoveRole() {
        createdRole = roleDao.create(role);
        roleDao.remove(role.getId());

        removeRole = roleDao.find(role.getId());

        assertNull(removeRole);
    }

    @Test
    public void testFindRoleById() {
        createdRole = roleDao.create(role);

        foundRole = roleDao.find(role.getId());

        assertEquals(createdRole, foundRole);
    }

    @Test
    public void testUpdateRole() {

        createdRole = roleDao.create(role);
        role.setName("Admin");
        updateRole = roleDao.update(role);

        assertEquals(role.getName(), updateRole.getName());
    }

}
